import json

def is_file_valid(file):
    """
    Return True if the file name is valid
    """
    return (file.endswith('.jpg') or file.endswith('.png')) and not file.startswith('.')


def get_json_metadata():
    """
    Get the json metadata
    """
    metadata = {}
    # Charger le fichier JSON dans un dictionnaire
    with open("json/metadata.json", "r") as f:
        # Charger le contenu JSON dans un dictionnaire
        metadata = json.loads(f.read())
    return metadata


def write_metadata(data):
    """
    write new metadata in json file
    """
    with open("json/metadata.json", "w") as f:
        json.dump(data, f, indent=4)
        
def get_user_preferences(user_id):
    # Charger les informations des utilisateurs depuis le fichier JSON
    with open('json/user_preference.json', 'r') as f:
        utilisateurs = json.loads(f.read())
    return utilisateurs

